# Class: netbox
class netbox (
  Array[String] $allowed_hosts,
  Hash[String, Variant[String, Integer]] $database,
  String $secret_key,

  Stdlib::Absolutepath $path = $netbox::params::path,
  String $source = $netbox::params::source,
  Optional[String] $revision = undef,
  String $owner = $netbox::params::owner,
  String $group = $netbox::params::group,

  Boolean $install_requirements = $netbox::params::install_requirements,

  Stdlib::Absolutepath $config_file = $netbox::params::config_file,

  Optional[Hash[String, String]] $admins = undef,
  Optional[String] $banner_top = undef,
  Optional[String] $banner_bottom = undef,
  Optional[String] $base_path = undef,
  Optional[Boolean] $debug = undef,
  Optional[Hash[String, Variant[String, Integer]]] $email = undef,
  Optional[Boolean] $enforce_global_unique = undef,
  Optional[Boolean] $login_required = undef,
  Optional[Boolean] $maintenance_mode = undef,
  Optional[String] $netbox_username = undef,
  Optional[String] $netbox_password = undef,
  Optional[Integer] $paginate_count = undef,
  Optional[Boolean] $prefer_ipv4 = undef,
  Optional[String] $timezone = undef,
  Optional[String] $date_format = undef,
  Optional[String] $short_date_format = undef,
  Optional[String] $time_format = undef,
  Optional[String] $short_time_format = undef,
  Optional[String] $datetime_format = undef,
  Optional[String] $short_datetime_format = undef,
) inherits netbox::params {
  contain ::netbox::install
  contain ::netbox::config
}
