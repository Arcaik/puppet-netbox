# Class: netbox::config
class netbox::config inherits netbox {
  assert_private()

  file { $netbox::config_file:
    ensure  => file,
    owner   => $netbox::owner,
    group   => $netbox::group,
    mode    => '0640',
    content => template('netbox/configuration.py.erb'),
    require => Vcsrepo['netbox'],
  }
}
