# Class: netbox::params
class netbox::params {
  $path = '/opt/netbox'
  $source = 'https://github.com/digitalocean/netbox.git'
  $owner = 'www-data'
  $group = 'www-data'

  $install_requirements = true

  $config_file = "${path}/netbox/netbox/configuration.py"
}
