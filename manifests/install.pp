# Class: netbox::install
class netbox::install inherits netbox {
  assert_private()

  file { $netbox::path:
    ensure => directory,
    owner  => $netbox::owner,
    group  => $netbox::group,
    mode   => '0755',
  }

  vcsrepo { 'netbox':
    ensure   => present,
    provider => 'git',
    path     => $netbox::path,
    source   => $netbox::source,
    revision => $netbox::revision,
    depth    => 1,
    owner    => $netbox::owner,
    group    => $netbox::group,
  }

  if $netbox::install_requirements {
    exec { 'netbox_requirements_install':
      command     => "pip install -r ${netbox::path}/requirements.txt",
      refreshonly => true,
      subscribe   => Vcsrepo['netbox'],
    }
  }
}
